package com.kasper;

import org.springframework.stereotype.Component;
@Component("dogBean")
public class Dog implements Pet {

    private String name;

    public Dog() {

        System.out.println("Dog bean is created");
    }

    @Override
    public void say() {
        System.out.println("Aww");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
