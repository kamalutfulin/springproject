package com.kasper;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.Serializable;

public class Test4 {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext2.xml");
        Dog dog2 = context.getBean("myPet",Dog.class);
        dog2.setName("вв");
        System.out.println(dog2.getName() );
//        dog2.setName("Belka");
//        Dog yourDog = context.getBean("myPet",Dog.class);
//        yourDog.setName("Strelka");
//        System.out.println(dog2.getName());
//        System.out.println(yourDog.getName());

//        System.out.println("Переменные ссылаются на один и тот же объект?" +
//                (dog2==yourDog));
//        System.out.println(dog2);
//        System.out.println(yourDog);
        context.close();
    }
}
